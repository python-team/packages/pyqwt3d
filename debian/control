Source: pyqwt3d
Section: python
Priority: optional
Maintainer: Gudjon I. Gudjonsson <gudjon@gudjon.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all-dev,
               python3-pyqt5,
               python3-sip-dev,
               python3-pyqt5.qtopengl,
               python3-numpy,
               pyqt5-dev,
               qt5-qmake,
               qtchooser,
               qtbase5-dev,
               libqt5opengl5-dev,
               libqwtplot3d-qt5-dev
Homepage: https://github.com/GauiStori/PyQwt3D
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/python-team/packages/pyqwt3d.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pyqwt3d

Package: python3-qwt3d-qt5
Conflicts: python-qwt3d-qt4
Replaces: python-qwt3d-qt4
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         ${sip3:Depends},
         python3-pyqt5.qtopengl,
         python3-numpy
Description: Python bindings of the QwtPlot3D library
 PyQwt3D is a set of Python bindings for the QwtPlot3D C++ class
 library which extends the Qt framework with widgets to visualize
 3-dimensional data.
 .
 This package contains the Python3 Qt5 bindings

Package: python-qwt3d-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Documentation for the Python-qwt3d library
 PyQwt3D is a set of Python bindings for the QwtPlot3D C++ class
 library which extends the Qt framework with widgets to visualize
 3-dimensional data. This package contains documentation and
 examples for both Qt3 and Qt4. The  examples show how easy it is
 to make a 3D plot and how to save a 3D plot to an image or an
 (E)PS/PDF file.
